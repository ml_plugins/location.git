# location

#### 介绍
适用于h5网站定位插件；基于h5定位、百度地图、高德地图、腾讯地图api封装的获取位置信息插件；选择使用哪个地图供应商传入对应的key即可，主要功能有根据ip定位、获取当前位置经纬度（gps、百度、高德、腾讯）、坐标转换、逆地址编码（根据经纬度获取具体位置信息）等

#### 使用说明
适用于h5网站定位插件


#### 主要功能
| 方法  | 方法描述  |
|---|---|
| getLocationByIp  | 通过ip获取位置信息  | 
|  getCurrentLngLat     | 获取当前位置经纬度   | 
| translateCoordinate  | GPS坐标转百度、高德、腾讯坐标     |  
| inverseGeocoding  | 逆地址编码（根据经纬度获取位置信息）  |   
| otherCoordTransfromToBaidu | 其它坐标转百度地图坐标 |  
| otherCoordTransfromToGaode | 其它坐标转高德地图坐标 |  
| otherCoordTransfromToTencenter | 其它坐标转腾讯地图坐标 | 
